#!/usr/bin/python

import numpy as np

f = open('data1', 'r');

first_line = f.readline();

size_pass=len(first_line)-1
size_file=len(f.readlines())+1

a = np.zeros((size_file,size_pass))

f.seek(0)

line = f.readline()

for y in range(size_file):
    for i in range(len(first_line) - 1):
        a[y,i]=ord(line[i])
    line=f.readline()

f.close();

alphabet=np.zeros((26))
result=""

for i in range(size_pass):
    alphabet=np.zeros((26))
    for y in range(size_file):
        ascii_number=a[y,i]-97
        alphabet[ascii_number]=alphabet[ascii_number] + 1
    alphabet[alphabet == 0] = 10000

    result = result + chr(np.argmin(alphabet) + 97)



print result
