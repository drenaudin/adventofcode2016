#!/bin/bash

src=$(pwd)

array=($(cat $src/data_day1 | grep -o '[RL][0-9][0-9]*'))
direction='H'
coord=(0 0)

for i in "${array[@]}"
do
    if [[ "$direction" == "H" ]]
    then
	if [[ $i =~ L.* ]]
	then
	    direction='G'
	else
	    direction='D'
	fi

    elif [[ "$direction" == "G" ]]
    then
	if [[ $i =~ L.* ]]
	then
	    direction='B'
	else
	    direction='H'
	fi

    elif [[ "$direction" == "D" ]]
    then
	if [[ $i =~ L.* ]]
	then
	    direction='H'
	else
	    direction='B'
	fi

    elif [[ "$direction" == "B" ]]
    then
	if [[ $i =~ L.* ]]
	then
	    direction='D'
	else
	    direction='G'
	fi
    fi

    number=$(echo $i | grep -o '[0-9][0-9]*')

   echo "${coord[@]} $direction"

    case $direction in
	"H")
	    coord[0]=$((${coord[0]}+$number))
	    ;;
	"B")
	    coord[0]=$((${coord[0]}-$number))
	    ;;
	"G")
	    coord[1]=$((${coord[1]}-$number))
	    ;;
	"D")
	    coord[1]=$((${coord[1]}+$number))
	    ;;
    esac
      

done

    echo "${coord[@]}, result : $((${coord[1]}+${coord[0]}))"
